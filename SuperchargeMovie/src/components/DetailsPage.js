import React, {useState} from "react";
import {
  SafeAreaView,
  FlatList,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  ScrollView,
  ActivityIndicator,
  Image
} from 'react-native';

import { Colors, } from 'react-native/Libraries/NewAppScreen';

import styles from "SuperchargeMovie/src/styles/detailsStyles";
import * as styleConst from "SuperchargeMovie/src/styles/styleConstants";

import * as Constants from "SuperchargeMovie/src/Constants";
import detailsStyles from "../styles/detailsStyles";

//  poster, title, overview

const DetailsPage = ({ route, navigation }) => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    const isDarkMode = useColorScheme() === 'dark';

    const { item } = route.params;

   
    //TODO make FetchService.js
    const getDetails = async () => {
      setLoading(true);
      let url = Constants.getDetailsUrl(item.id);
      try {
        const response = await fetch(url);
        const json = await response.json();
        setData(json);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    }

    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };

    React.useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
        getDetails();
      });
      return unsubscribe;
    }, [navigation]);
    
  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      {isLoading ? 
      <ActivityIndicator/> : 
      (
        <ScrollView style={detailsStyles.pageContainer}>
          <Image
            style={detailsStyles.poster}
            source={{
              uri: Constants.getImageUrl("w500",item.poster_path),
            }}
          />
          <Text style={detailsStyles.itemTitle}>{item.title}</Text>
          <Text style={detailsStyles.itemOriginalTitle}>{item.original_title}</Text>
          <Text style={detailsStyles.itemOverview}>{item.overview}</Text>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default DetailsPage;
