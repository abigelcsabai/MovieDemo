import React, {useState} from "react";
import {
  SafeAreaView,
  FlatList,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableHighlight,
  TextInput,
  ActivityIndicator,
  Image
} from 'react-native';

import { Colors, } from 'react-native/Libraries/NewAppScreen';
import * as Constants from "SuperchargeMovie/src/Constants";
import * as styleConst from "SuperchargeMovie/src/styles/styleConstants";
import listStyles from "SuperchargeMovie/src/styles/listStyles";

const searchIcon = require("SuperchargeMovie/src/resources/SearchIcon.png")
const ArrowIcon = require("SuperchargeMovie/src/resources/ArrowIcon.png")

const ListPage = ({ navigation }) => {

    const isDarkMode = useColorScheme() === 'dark';

    const backgroundStyle = {backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,};

    const [searchText, setSearchInput] = useState("");
    const [isLoading, setLoading] = useState(false);
    const [firstSearch, setFirstSearch] = useState(false);
    const [data, setData] = useState([]);

    const onItemPress = (item) => {
      navigation.navigate('Movie Details', { item: item })
    }

    const onSearchPress = (searchText) => {
      if(!firstSearch){
        setFirstSearch(true)
      } 
      setLoading(true);
      getMoviesFromApiAsync(searchText);
    }

    //TODO make FetchService.js
    const getMoviesFromApiAsync = async (searchText) => {
      let url = Constants.getListUrl(searchText);
      try {
        const response = await fetch(url);
        const json = await response.json();
        setData(json);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    }

    const renderItem = ({ item, index }) => (
        <View style={listStyles.listItemContainer}>
            <TouchableHighlight onPress={() => onItemPress(item)} activeOpacity={0.5} underlayColor="#FFFFFF">
              <View style={listStyles.listItem}>
                <Image
                  style={listStyles.listItemPoster}
                  source={{
                    uri: Constants.getImageUrl("w500",item.poster_path),
                  }}
                />
                <View style={{flex: 1}}>
                  <Text style={listStyles.listItemTitle}>{item.title}</Text>
                  <Text style={listStyles.listItemOriginalTitle}>{item.original_title}</Text>
                  <Text style={listStyles.listItemPopularity}>{"popularity: "+item.popularity}</Text>
                </View>
                <View style={listStyles.arrowContainer}>
                  <Image
                    style={listStyles.arrowIcon}
                    source={ArrowIcon}
                  />
                </View>
                
              </View>
            </TouchableHighlight>
        </View>
    );

    function renderContent(){
      return(
        isLoading ? 
        <ActivityIndicator/> : 
        renderList()
      )
      
    }

    function renderList(){
      return(
        !isLoading && (data.results!=null || data.results!=undefined) && data.results.length!=0  ?
        ( <View>
          <FlatList
            data={data.results}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
        ) : (
        <View style={listStyles.informationContainer}>
            <Text>No Resul</Text>
          </View>
        )
      )
    }
    
  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={listStyles.pageContainer}>
        <View style={listStyles.searchContainer}>
          <TextInput 
            style={listStyles.searchInput}
            placeholder='Search Movies' 
            placeholderTextColor={"grey"}
            onChangeText={(searchText) => {setSearchInput(searchText)}}
            value={searchText} 
          />
          <TouchableHighlight onPress={()=>{onSearchPress(searchText)}}>
            <Image
              style={listStyles.searchIcon}
              source={searchIcon}
            />
          </TouchableHighlight>
          
        </View>
        {firstSearch ? 
        renderContent() : 
        (
          <View style={listStyles.informationContainer}>
            <Text>Your search result will be apear here.</Text>
          </View>
        )
        }
      </View>
    </SafeAreaView>
  );
};

export default ListPage;
