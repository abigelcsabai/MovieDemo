const apiKey = "?api_key=43a7ea280d085bd0376e108680615c7f"

const baseUrl = "https://api.themoviedb.org/3"

const ListUrl = "/search/movie"+apiKey+"&query="

const DetailsUrls = "/movie/";

const ImageBaseUrl = "https://image.tmdb.org/t/p/"

export const getListUrl = (searchText) => {
    return baseUrl + ListUrl + searchText;
} 

export const getDetailsUrl = (id) =>{
    return baseUrl + DetailsUrls + id + apiKey;
}

//todo: file not found in all link
export const getImageUrl = (size, path) =>{
    return ImageBaseUrl + size + path;
}