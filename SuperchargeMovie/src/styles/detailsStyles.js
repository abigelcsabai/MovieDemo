import { StyleSheet, useWindowDimensions } from 'react-native';
import * as styleConst from "SuperchargeMovie/src/styles/styleConstants";
//TODO resolution fix
//const { height, width } = useWindowDimensions();


export default StyleSheet.create({
  pageContainer:{
 
  },
  itemTitle:{
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 5,
  },
  itemOriginalTitle:{
    textAlign: "center",
    fontSize: 14,
    marginLeft: 5,
  },
  itemOverview:{
    textAlign: "justify",
    marginLeft: 5,
    fontSize: 10,
  },
  poster:{
    width: 300, 
    height: 300, 
    backgroundColor: "black", 
    alignSelf: "center", 
    margin: 20
  }
});
