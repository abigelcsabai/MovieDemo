import { StyleSheet, useWindowDimensions } from 'react-native';
import * as styleConst from "SuperchargeMovie/src/styles/styleConstants";
//TODO resolution fix
//const { height, width } = useWindowDimensions();


export default StyleSheet.create({
    pageContainer:{
    },
    listItemContainer:{
        
        height: 100,
        backgroundColor: styleConst.listItemContainerBackgroundColor,
        borderRadius: 20,
        margin: 20,
    },
    listItem:{
        flexDirection: "row",
        margin: 5,
    },
    searchContainer:{
        margin: 20,
        borderBottomWidth: 2,
        borderBottomColor: styleConst.searchContainerBorderBottomColor,
        flexDirection: "row"
    },
    listItemPoster:{
        width: 50, 
        height: 70,
        margin: 8,
        backgroundColor: "black",
    },
    searchInput:{
        flex: 1,
    },
    listItemTitle:{
        fontSize: 16,
        fontWeight: "bold",
        marginLeft: 5,
    },
    listItemOriginalTitle:{
        fontSize: 14,
        marginLeft: 5,
    },
    listItemPopularity:{
        marginLeft: 5,
        fontSize: 10,
    },
    
    arrowContainer:{ 
        alignSelf: "center",
        justifyContent: "center"
    },
    arrowIcon:{
        margin: 5,
        height: 20, 
        width: 20,
    },
    informationContainer:{
        alignItems: "center",
        justifyContent: "center"
    },
    searchIcon:{
        width: 40, 
        height: 40
    }
   

});