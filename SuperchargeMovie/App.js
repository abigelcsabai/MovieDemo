/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';


import ListPage from 'SuperchargeMovie/src/components/ListPage';
import DetailsPage from 'SuperchargeMovie/src/components/DetailsPage';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Search Page" component={ListPage} />
        <Stack.Screen name="Movie Details" component={DetailsPage} />
      </Stack.Navigator>
    </NavigationContainer>
    
  );
};

export default App;
